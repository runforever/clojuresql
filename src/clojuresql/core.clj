(ns clojuresql.core)

;;; jdbc 驱动
(require '[clojure.java.jdbc :as jdbc])

;;; 数据库配置
(def db-spec {
              :subprotocol "mysql"
              :subname "//172.17.8.101:3306/didi_db"
              :user "admin"
              :password "root"})

;;; Select 语句处理
(defn select
  [fields]
  (clojure.string/join
   " "
   (concat
    ["SELECT"]
    [(clojure.string/join
    ", "
    fields)])))

;;; from 语句处理
(defn from
  [& table]
  (clojure.string/join " " (concat ["FROM"] table)))

;;; 定义控制结构
(defmacro cljsql
  [& sql_atom]
  `(jdbc/query
    db-spec
    [(clojure.string/join " " (list ~@sql_atom))])
  )

;;; 使用
(cljsql (select ["username", "password"])
        (from "auth_user"))
